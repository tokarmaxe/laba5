package com.example.maxim.laba5;

import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    public String alfabet = "abcdefghijklmnopqrstuvwxyz";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickEnc(View view) {
        EditText stringET = (EditText) findViewById(R.id.str);
        EditText keyET = (EditText) findViewById(R.id.key);
        if(stringET.getText().toString().length() != 3)
        {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Ваше сообщение не состоит из 3х букв!",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else if(keyET.getText().toString().length() != 9)
        {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Ваш ключ не состоит из 9ти букв!",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else{
            encrypt(stringET.getText().toString().toUpperCase(), keyET.getText().toString().toUpperCase());
        }
    }

    public void encrypt(String str, String key) {
        char[] mas_str = str.toCharArray();
        char[] mas_key = key.toCharArray();

        //fill vectors h1,h2,h3
        int[] h1 = new int[3];
        int[] h2 = new int[3];
        int[] h3 = new int[3];
        for (int i = 0; i<mas_key.length;i++)
        {
            if(i<=2)
                h1[i] = ((int)mas_key[i])-65;
            else if(i>2 && i<=5)
                h2[i-3] = ((int)mas_key[i])-65;
            else
                h3[i-6] = ((int)mas_key[i])-65;
        }

        //mas_str convert to int mas_strInt
        int[] mas_strInt = new int[3];
        for (int i = 0; i < mas_str.length; i++)
        {
            mas_strInt[i] = ((int)mas_str[i])-65;
        }

        int[] p = new int[3];
        p[0] = myFunc(h1,mas_strInt);
        p[1] = myFunc(h2,mas_strInt);
        p[2] = myFunc(h3,mas_strInt);

        char[] res = new char[3];
        for (int i = 0; i<3; i++)
        {
            res[i] = (char)(p[i]+65);
        }

        String newStr = new String(res);
        display(newStr);
    }

    public int myFunc(int[] h, int[] mas_strInt)
    {
        int value = 0;
            for (int j = 0; j<3; j++)
            {
                value += (h[j] * mas_strInt[j]);

            }
        return value%alfabet.length();
    }

    public void display(String result) {
        TextView ownView = (TextView) findViewById(R.id.result);
        ownView.setText(result);
    }
}
